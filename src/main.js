import '@/styles/common.scss'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { useIntersectionObserver } from '@vueuse/core'
import { lazyPlugin } from './directive/index'
import { componentPlugins } from './components/index'
// 依赖pinia
import { createPinia } from 'pinia'
// 引入数据持久化插件
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
const app = createApp(App)
// 注册
const pinia = createPinia()
app.use(pinia)
pinia.use(piniaPluginPersistedstate)

app.use(router)
app.use(lazyPlugin)
app.use(componentPlugins)
app.mount('#app')
   //利用pinia持久化插件将token保存到本地内存，做数据持久化处理
//定义全局组件