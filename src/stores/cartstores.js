
import { computed, ref } from 'vue'
import { useUserStore } from './users'
import { defineStore } from 'pinia'
import { insertCartAPI, findCaterlistAPI,delCartAPI } from '@/apis/cart'

 /**
  * 
  * 
  * import pinia from '@/store/pinia.js'
import { useStore } from '@/store'
const store = useStore(pinia)
  */
export const useCartStore = defineStore(('cart'), () => {
  const useStore = useUserStore()
  // 1. 定义state - cartList
  const cartList = ref([])
  //判断用户是否登陆，如果登陆就走登陆接口逻辑进行添加，非登录就走一般逻辑

  const isLogin = computed(() => useStore.userInfo.token)

  //更新购物车商品列表
  const updateCateStore=async()=>{
    const res =await findCaterlistAPI()
    cartList.value=res.result
  }
  //合并购物车列表
  // const MergecartList=async()=>{

  // }
  // 2. 定义action - addCart
  const addCart = async (goods) => {
    const { skuId, count } = goods
    if (isLogin.value) {
      console.log('11',{skuId,count})
      await insertCartAPI({ skuId, count })
      updateCateStore()
    }
    else {
      // console.log(useStore.userInfo)
      // 添加购物车操作
      // 已添加过 - count + 1
      // 没有添加过 - 直接push
      // 思路：通过匹配传递过来的商品对象中的skuId能不能在cartList中找到，找到了就是添加过
      const item = cartList.value.find((item) => goods.skuId === item.skuId)
      if (item) {
        // 找到了
        item.count++
      } else {
        // 没找到
        cartList.value.push(goods)
      }
    }
  }
  // 删除购物车
  const delCart =async (skuId) => {
    if(isLogin.value){
      await delCartAPI([skuId])
      updateCateStore()
    }
    else{
         // 思路：
    // 1. 找到要删除项的下标值 - splice
    // 2. 使用数组的过滤方法 - filter
    const idx = cartList.value.findIndex((item) => skuId === item.skuId)
    cartList.value.splice(idx, 1)
    }

  }
  //退出登陆，清空购物车
  const clearList=()=>{
    cartList.value=[]
  }
  //商品数
  const allcount = computed(() => cartList.value.reduce((a, c) => a + c.count, 0))
  const allprice = computed(() => cartList.value.reduce((a, c) => a + c.count * c.price, 0))
  // 单选功能
  const singleCheck = (skuId, selected) => {
    // 通过skuId找到要修改的那一项 然后把它的selected修改为传过来的selected
    const item = cartList.value.find((item) => item.skuId === skuId)
    item.selected = selected
  }
  //全选功能action
  const allCheck = (selected) => {
    // 把cartList中的每一项的selected都设置为当前的全选框状态
    cartList.value.forEach(item => item.selected = selected)
  }
  //是否全选计算属性
  const isAll = computed(() => cartList.value.every((item) => item.selected))
  //已选择商品的数量
  const selectedCount = computed(() => cartList.value.filter(item => item.selected).reduce((a, c) => a + c.count, 0))
  // 4. 已选择商品价钱合计
  const selectedPrice = computed(() => cartList.value.filter(item => item.selected).reduce((a, c) => a + c.count * c.price, 0))
  return {
    cartList,
    addCart,
    delCart,
    allcount,
    allprice,
    singleCheck,
    isAll,
    clearList,
    allCheck,
    selectedCount,
    selectedPrice,
    updateCateStore
  }
}, {
  persist: true,
})