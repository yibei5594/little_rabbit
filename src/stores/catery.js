import { ref, computed ,onMounted } from 'vue'
import { defineStore } from 'pinia'
import {getnav} from '@/apis/layout'
const catelist=ref([])
export const useCateryStore = defineStore('catery', () => {
    const getnavlist=async ()=>{
      const res=await getnav()
      catelist.value=res.result
    }
    return{
        catelist,
        getnavlist
    }
    //利用pinia持久化插件将token保存到本地内存，做数据持久化处理
})
