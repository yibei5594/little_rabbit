import axios from 'axios'
import { ElMessage } from 'element-plus'
import 'element-plus/theme-chalk/el-message.css'
import {useUserStore} from '@/stores/users'
import { useRouter } from 'vue-router'
const router=useRouter()
// 创建axios实例
const http = axios.create({
  baseURL: 'http://pcapi-xiaotuxian-front-devtest.itheima.net',
  timeout: 5000
})
// axios请求拦截器
http.interceptors.request.use(config => {
    // 1. 从pinia获取token数据
    const userStore = useUserStore()
    // 2. 按照后端的要求拼接token数据
    const token = userStore.userInfo.token
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  }, e => Promise.reject(e))
  
  // axios响应式拦截器
  http.interceptors.response.use(res => res.data, e => {
    ElMessage({
      type:'error',
      message:'warong'
    })
      // 1. 从pinia获取token数据
      const userStore = useUserStore()
      if(e.response.status===401)
      {
        userStore.clearUserInfo()
        //跳转到登陆页面
        router.push('/login')
      }
    return Promise.reject(e)
  })
  
  
  export default http