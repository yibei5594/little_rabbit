import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/views/Layout/index.vue'
import Home from '@/views/Home/index.vue'
import Category from '@/views/Category/index.vue'
import SubCategory from '@/views/SubCatery/index.vue'
import Detail from '@/views/Details/index.vue'
import Login from '@/views/Login/index.vue'
import CartList from '@/views/CartList/index.vue'
import CheckOut from '@/views/Checkout/index.vue'
import Pay from '@/views/PayOff/index.vue'
import PayBack from '@/views/PayOff/PayBack.vue'
import Member from '@/views/Member/index.vue'
import UserInfo from '@/views/Member/UserInfo.vue'
import OrderInfo from '@/views/Member/OrderInfo.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path:'/',
      component:Layout,
      children:
      [
        {
        path:'',
        component:Home
        },
        {
          path:'category/:id',
          component:Category
        },
        {
          path:'category/sub/:id',
          component:SubCategory
        },
        {
          path: 'detail/:id',
          component: Detail
        },
        {
          path: '/cartlist',
          component: CartList
        },
        {
          path: '/checkout',
          component: CheckOut
        },
        {
          path:'/pay',
          component:Pay
        },
        {
          path:'/paycallback',
          component:PayBack
        },
        {
          path:'/member',
          component:Member,
          children:[
            {
              path:'',
              component:UserInfo
            },
            {
              path:'order',
              component:OrderInfo
            }
          ]

        }
      ]
    },
    {
      path:'/login',
      component:Login
    }
  ],
  //添加路由滚动行为定制
  scrollBehavior(){
    return{
      top:0
    }
  }

})

export default router
