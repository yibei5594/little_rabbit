import XtxSku from './XtxSku/XtxSku.vue'
import ImageView from './ImageView/index.vue'
export const componentPlugins={
    install(app){
        app.component('XtxGoodSku',XtxSku)
        app.component('XtxImageView',ImageView)
    }
}