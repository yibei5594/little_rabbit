import {computed, onUnmounted, ref} from 'vue'
import  dayjs  from 'dayjs'
export const useCountDwon=()=>{
    let timer
    const time=ref(0)
    const formatTime=computed(()=>dayjs.unix(time.value).format('mm分ss秒'))
    const start=(nowtime)=>{
        time.value=nowtime
        timer=setInterval(()=>{
            time.value--
        },1000)
    }
    onUnmounted(()=>{
        timer&&clearInterval(timer)
    })
    return {
        start,
        formatTime
    }
}