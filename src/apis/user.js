import request from "@/utils/http";
export const loginAPI=({account,password})=>{
    return request({
        url:'/login',
        method:'POST',
        data:{
            account,
            password
        }
    })
}
//猜你喜欢接口封装
export const getLikeListAPI = ({ limit }) => {
    return request({
      url:'/goods/relevant',
      params: {
        limit 
      }
    })
  }