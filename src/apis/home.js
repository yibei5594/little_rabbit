import httpInstance from '@/utils/http.js'
//封装轮播图接口
//  export function getBannerApi(){
//     return httpInstance({
//         url:'/home/banner'
//     })
//  }
 export function getBannerApi (params = {}) {
  // 默认为1 商品为2
  const { distributionSite = '1' } = params
  return httpInstance({
    url: '/home/banner',
    params: {
      distributionSite
    }
  })
}
 //封装新鲜好物接口
 export const findNewAPI = () => {
    return httpInstance({
      url:'/home/new'
    })
  }
  //封装热榜接口
  export const getHotAPI = () => {
    return  httpInstance({
        url:'home/hot'
    }
    )
  }
  //封装产品列表接口
  export const getGoodsAPI = () => {
    return httpInstance({
      url: '/home/goods'
    })
  }
