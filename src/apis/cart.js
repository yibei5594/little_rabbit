import request from '@/utils/http'
//添加购物车接口
export const insertCartAPI = ({ skuId, count }) => {
  return request({
    url: '/member/cart',
    method: 'POST',
    data: {
      skuId,
      count
    }
  })
}
  //获得购物车列表接口
  export const findCaterlistAPI=()=>{
    return request({
        url:'/member/cart'
    })
  }
  //删除购物车列表
  export const delCartAPI = (ids) => {
    return request({
      url: '/member/cart',
      method: 'DELETE',
      data: {
        ids
      }
    })
  }
  //合并购物车接口封装
  export const mergeCartAPI=(data)=>{
    return request({
      url: '/member/cart/merge',
      method: 'POST',
      data
    })
  }